#include<iostream>
using namespace std;
class vectors
{	public:
	int x;
	int y;
	int z;
	friend istream& operator >>(istream& is,vectors& obj)
	{	cout<<"Enter x coordinate:";
	 	is>>obj.x;
		cout<<"Enter y coordinate:";
		is>>obj.y;
		cout<<"Enter z coordinate:";
	 	is>>obj.z;
		return is;
	}
	friend ostream& operator <<(ostream& os,vectors& obj)
	{	cout<<"("<<obj.x<<")i+"<<"("<<obj.y<<")j+"<<"("<<obj.z<<")k"<<endl;
		return os;
	}
	friend vectors operator +(vectors a,vectors b)
	{	vectors result;
		result.x=a.x+b.x;
		result.y=a.y+b.y;
		result.z=a.z+b.z;
		return result;
	}
	friend vectors operator *(vectors a,vectors b)
	{	vectors result;
		result.x=a.x*b.x;
		result.y=a.y*b.y;
		result.z=a.z*b.z;
		cout<<"Dot product:"<<result.x+result.y+result.z<<endl;
                return result;
	}
        friend vectors operator ^(vectors a,vectors b)
	{	vectors result;
                result.x=a.y*b.z-a.z*b.y;
		result.y=a.x*b.z-a.z*b.x;
		result.z=a.x*b.y-a.y*b.x;
		return result;
	}
};
int main()
{	vectors obj1,obj2;
	cin>>obj1;
	cin>>obj2;
	cout<<"The vectors are"<<endl;
	cout<<obj1;
	cout<<obj2;
	vectors obj3;
	obj3=obj1+obj2;
	cout<<"Sum is:";
	cout<<obj3;
	vectors obj4;
	obj4=obj1*obj2;
	vectors obj5;
	obj5=obj1^obj2;
	cout<<"Vector product is:";
        cout<<obj5;
	return 0;
}


